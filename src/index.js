import express from 'express';
import path from 'path';

const app = express();

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname + '/index.html'));
})

app.post('/api', (req, res) => {
  res.status(400).json({ global: { error: 'invalid api req' } });
})

app.listen(8080, () => console.log('server is running on http://localhost:8080'));